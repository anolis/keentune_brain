# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
import os
import tornado
import logging

from brain.controller import tuning, sensitize
from brain.common.config import Config

"""
AI Engine

RESTFUL API:
    /init    : Initialize the optimizer.
    /acquire : Acquire a configuration candidate.
    /feedback: Feedback benchmark performance score to latest configuration candidate.
    /best    : Get best configuration.
    /end     : End the optimizer active.
WEB API
    /sensitize : Calculate sensitivity of parameters.
    /sensitize_list  : List numpy data file which can used for calculating sensitivity of parameters.
    /sensitize_delete: Remove numpy data file.
"""

logger = logging.getLogger('common')


def main():
    app_brain = tornado.web.Application(handlers=[
        (r"/init", tuning.InitHandler),
        (r"/acquire", tuning.AcquireHandler),
        (r"/feedback", tuning.FeedbackHandler),
        (r"/best", tuning.BestHandler),
        (r"/end", tuning.EndHandler),
        (r"/sensitize", sensitize.SensitizeHandler),
        (r"/avaliable", sensitize.AvaliableHandler),
        (r"/terminate", sensitize.TerminateHandler),
        (r"/sensitize_delete", sensitize.DataDeleteHandler),
    ])
    http_server_brain = tornado.httpserver.HTTPServer(app_brain)
    http_server_brain.listen(Config.BRAIN_PORT)
    
    logger.info("keentune-brain running")
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        os.exit(0)