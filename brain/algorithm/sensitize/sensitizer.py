# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
import logging
import numpy as np

from typing import List
from brain.common.pylog import functionLog
 
logger = logging.getLogger('common')


class Sensitizer(object):
    @functionLog
    def __init__(self,
                 X: np.ndarray,
                 y: np.ndarray,
                 params: List[str],
                 seed: int=None,
                 learner_name: str='xgboost',
                 explainer_name: str='Xsen',
                 epoch: int=100):
        """Initializer
        Args:
             X (np.ndarray): data for analyzing sensitivity scores
             y (np.ndarray): benchmark scores for analyzing sensitivity scores
             params (list): a list of parameter names
             seed (int): random seed
             learner_name (string): name of learner used for explaining
             explainer_name (string): name of explainer
             epoch: number of epochs to train the learner
        """
        self.X = X
        self.y = y
        self.params = params
        self.seed = seed if seed is not None else 42
        self.learner_name = learner_name
        self.explainer_name = explainer_name
        self.epoch = epoch
        self.sensi = np.zeros(len(params))
        # shap family and xsen needs a reference point as the baseline for explanation, initialize here
        self.base_x = self.X[0]
 
        self.learner = None
        self.learner_performance = np.inf

        self._build_explainer()

    @functionLog
    def _build_learner(self):
        """Build and train learning model for analyzing sensitivity scores
            Args:
                X (numpy array): parameter value data
                y (numpy arary): benchmark scores for regression
        """
        # build learning model with TRAINING data
        from brain.algorithm.sensitize.sensiModel import Learner

        self.learner = Learner(name=self.learner_name,
                               seed=self.seed,
                               epoch=self.epoch)
        self.learner_performance = self.learner.run(X=self.X, y=self.y)


    @functionLog
    def _build_explainer(self):
        """Call explainer model to analyze sensitivity scores
        Args:
            X (numpy array): parameter value data
            y (numpy arary): benchmark scores
        """
        # build explaining model with ALL data
        if self.explainer_name == "Xsen":
            from brain.algorithm.sensitize.sensiModel import XsenExplainer
            self.explainer = XsenExplainer(name=self.explainer_name, seed=self.seed)
            self._build_learner()
        
        elif self.explainer_name == "SHAPKernel":
            from brain.algorithm.sensitize.sensiModel import SHAPKernelExplainer
            self.explainer = SHAPKernelExplainer(name=self.explainer_name, seed=self.seed)
            self._build_learner()
        
        elif self.explainer_name == "XGBTotalGain":
            from brain.algorithm.sensitize.sensiModel import XGBTotalGainExplainer
            self.explainer = XGBTotalGainExplainer(name=self.explainer_name, seed=self.seed)
            self._build_learner()
        
        elif self.explainer_name == "LASSO":
            from brain.algorithm.sensitize.sensiModel import LassoExplainer
            self.explainer = LassoExplainer(name=self.explainer_name, seed=self.seed)
            
        elif self.explainer_name == "MI":
            from brain.algorithm.sensitize.sensiModel import MIExplainer
            self.explainer = MIExplainer(name=self.explainer_name, seed=self.seed)
            
        elif self.explainer_name == "GP":
            from brain.algorithm.sensitize.sensiModel import GPExplainer
            self.explainer = GPExplainer(name=self.explainer_name, seed=self.seed)
        
        else:
            # use xsen as default
            from brain.algorithm.sensitize.sensiModel import XsenExplainer
            logger.info(f"Input explainer {self.explainer_name} not supported, use the default Xsen")
            self.explainer_name = "Xsen"
            self.explainer = XsenExplainer(name=self.explainer_name, seed=self.seed)
            self._build_learner()


    @functionLog
    def analyze(self):
        """Call explainer model to analyze sensitivity scores
        Args:
            X (numpy array): parameter value data
            y (numpy arary): benchmark scores for regression
        Return:
            normalized sensitivity scores
        """
        sensi = self.explainer.run(learner=self.learner,
                                   params=self.params,
                                   X=self.X,
                                   y=self.y,
                                   base_x=self.base_x)
        
        if self.explainer_name in ["GP","LASSO"]:
            self.learner_performance = self.explainer.performance
        if self.explainer_name == "MI":
            self.learner_performance = 1.0
        
        return sensi