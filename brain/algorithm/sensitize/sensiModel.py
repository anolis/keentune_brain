# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
import logging
import collections
import numpy as np
import scipy as sp

from copy import deepcopy
from abc import ABCMeta, abstractmethod
from functools import partial
from typing import Any, Optional, List
 
from brain.common.pylog import functionLog
 
logger = logging.getLogger('common')
 
class Learner(object):
    @functionLog
    def __init__(self,
                 name: str,
                 seed: int,
                 epoch: int=100):
        """Initializer
           Args: name (string): name of learning model, only support xgboost for now
        """
        self.name = name
        self.seed = seed
        self.epoch = epoch
        # learning model before fitting
        if name == "xgboost":
            try:
                from xgboost import XGBRegressor
            except ImportError:
                raise ImportError('XGBRegressor is not available')
 
            # learning model before fitting
            self.model = XGBRegressor(objective='reg:linear',
                                      verbosity=0,
                                      random_state=self.seed,
                                      predictor="cpu_predictor")
            # auto tuning parameters for the learning model
            self.params = {'n_estimators': [50, 100, 200],
                           'learning_rate': [0.01, 0.1, 0.2, 0.3],
                           'max_depth': range(3, 10),
                           'colsample_bytree': [0.6, 0.8, 1.0],
                           'reg_alpha': [0.1, 1.0, 25.0, 100.0],
                           'reg_lambda': [0.1, 1.0, 25.0, 100.0],
                           'min_child_weight': [1, 5, 10],
                           'subsample': [0.6, 0.8, 1.0]}
        else:
            logger.info("Support xgboost for now, current learner {} is not supported".format(self.name))
            raise NotImplementedError
 
    @functionLog
    def run(self,
            X:np.ndarray,
            y:np.ndarray) -> float:
        """Implementation of training and evaluating learning model
            Args:
                X_train (numpy array): training data
                y_train (numpy arary): training label for regression
                X_test (numpy array): test data
                y_test (numpy arary): test label for regression
        """
        from sklearn.model_selection import RandomizedSearchCV, KFold
        from sklearn.model_selection import train_test_split
        from sklearn.metrics import mean_absolute_percentage_error
 
        # split training and testing data for building learning and explaining models
        X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                            train_size=0.75,
                                                            random_state=self.seed)
 
        scoring = "neg_mean_absolute_error"  # "neg_mean_absolute_percentage_error"
        scoring_func = mean_absolute_percentage_error
        kfold = KFold(n_splits=5, shuffle=True, random_state=self.seed)
        tuner = RandomizedSearchCV(estimator=self.model,
                                   param_distributions=self.params,
                                   n_iter=self.epoch,
                                   scoring=scoring,
                                   cv=kfold.split(X_train, y_train),
                                   verbose=0,
                                   random_state=self.seed)
        tuner.fit(X_train, y_train.ravel())
        
        self.model = tuner.best_estimator_
        performance = scoring_func(y_test.ravel(), self.model.predict(X_test))
        return performance
 
 
class Explainer(metaclass=ABCMeta):
    @functionLog
    def __init__(self, name:str, seed:int):
        """Initializer for all explainable methods
            Args:
                name (string): name of explaining model
                seed (int): random seed used for certain method for reproducibility
        """
        self.name = name
        self.seed = seed
 
    @abstractmethod
    def run(self,
            learner: Any,
            params: List[str],
            X: np.ndarray,
            y: Optional[np.ndarray] = None,
            base_x: Optional[np.ndarray] = None) -> np.ndarray:
        """Implementation explaining learning models.
            Sub-class is expected to implement this method to return sensitivity scores
            Args:
                learner (model): a learning model for explaining
                X (numpy array): explaining data
                params (list): a list of parameter names
            Return:
                sensi values (numpy array)
        """
        raise NotImplemented("Explainer.run() is not implemented.")
 
 
class XsenExplainer(Explainer):
    @functionLog
    def __init__(self, name: str, seed: int):
        """Initializer, only used for explaining nonlinear learning models
            Args: name (string): name of explaining model, support SHAP, MI, and xsen
                  xsen is a combined method with SHAP and MI
        """
        super().__init__(name, seed)
        self.name = name
        if name != "Xsen":
            logger.info("Selected Xsen but the current explainer {} is specified".format(name))
            self.name = "Xsen"
        self.seed = seed
 
    @functionLog
    def run(self,
            learner: Any,
            params: List[str],
            X: np.ndarray,
            y: Optional[np.ndarray] = None,
            base_x: Optional[np.ndarray] = None) -> np.ndarray:
        """Implementation explaining nonlinear learning models
            Args:
                learner (model): a learning model for explaining
                X (numpy array): explaining data
                params (list): a list of parameter names
            Return:
                sensi values (numpy array)
        """
        try:
            from shap import KernelExplainer
        except ImportError:
            logger.info('shap.KernelExplainer is not available')
            raise ImportError('shap.KernelExplainer is not available')
        # sample data for computing shaply values
        background = np.reshape(base_x, (
            1, len(base_x)))
        # shap.kmeans(X, 10) if X.shape[0]>10 else shap.kmeans(X, X.shape[0])
        # use kernel explainer, which compute shaply values by kernel approximation,
        # (1) this method is slower but more accurate
        # (2) this method is more general and be applied to different learning models
        # other choices could consider tree explainer, which is designed for trees
        explainer = KernelExplainer(learner.model.predict, background)
        sensi = explainer.shap_values(X)
        sensi = np.mean(np.nan_to_num(sensi), axis=0).squeeze()
 
        # use univariate values as gates, taking values from 1.0 to 2.0
        # most univariate values are very small,
        # need to scale up (by 2 for now), otherwise tanh(x) will be too small
        sensi_scaler = MIExplainer(name="MI",seed=self.seed)
        weights = sensi_scaler.run(learner=None,
                                   params=params,
                                   X=X,
                                   y=y,
                                   base_x=base_x)
 
        sensi = sensi * np.tanh(2.0 * weights) + 1.0
        sensi = sensi / np.sum(np.abs(sensi))
        sensi = np.nan_to_num(sensi)
        return sensi


class SHAPKernelExplainer(Explainer):
    @functionLog
    def __init__(self, name:str, seed:int):
        """Initializer, only used for explaining nonlinear learning models
            Args: name (string): name of explaining model
        """
        super().__init__(name, seed)
        self.name = name
        if name != "SHAPKernel":
            logger.info("Selected SHAPKernel but the current explainer {} is specified".format(name))
            self.name = "SHAPKernel"
        self.seed = seed
 
    @functionLog
    def run(self,
            learner: Any,
            params: List[str],
            X: np.ndarray,
            y: Optional[np.ndarray] = None,
            base_x: Optional[np.ndarray] = None) -> np.ndarray:
        """Implementation explaining nonlinear learning models
            Args:
                learner (model): a learning model for explaining
                X (numpy array): explaining data
                params (list): a list of parameter names
            Return:
                sensi values (numpy array)
        """
        try:
            from shap import KernelExplainer
        except ImportError:
            logger.info('shap.KernelExplainer is not available')
            raise ImportError('shap.KernelExplainer is not available')
        # sample data for computing shaply values
        background = np.reshape(base_x, (
            1, len(base_x)))
        # shap.kmeans(X, 10) if X.shape[0]>10 else shap.kmeans(X, X.shape[0])
        # use kernel explainer, which compute shaply values by kernel approximation,
        # (1) this method is slower but more accurate
        # (2) this method is more general and be applied to different learning models
        # other choices could consider tree explainer, which is designed for trees
        explainer = KernelExplainer(learner.model.predict, background)
        sensi = explainer.shap_values(X)
        sensi = np.mean(np.nan_to_num(sensi), axis=0).squeeze()
        sensi = sensi / np.sum(np.abs(sensi))
        sensi = np.nan_to_num(sensi)
        return sensi
    

class XGBTotalGainExplainer(Explainer):
    @functionLog
    def __init__(self, name:str, seed:int):
        """Initializer, only used for explaining nonlinear learning models
            Args: name (string): name of explaining model.
            use xgboost's self-explaining method, which support :
            - 'weight': the number of times a feature is used to split the data across all trees.
            - 'gain': the average gain across all splits the feature is used in.
            - 'cover': the average coverage across all splits the feature is used in.
            - 'total_gain': the total gain across all splits the feature is used in.
            - 'total_cover': the total coverage across all splits the feature is used in.
            use total_gain as default
        """
        super().__init__(name, seed)
        self.name = name
        if self.name != "XGBTotalGain":
            logger.info("Selected XGBTotalGain but the current explainer {} is specified".format(name))
            self.name = "XGBTotalGain"
        self.seed = seed
 
    @functionLog
    def run(self,
            learner: Any,
            params: List[str],
            X: np.ndarray,
            y: Optional[np.ndarray] = None,
            base_x: Optional[np.ndarray] = None) -> np.ndarray:
        """Implementation explaining nonlinear learning models
            Args:
                learner (model): a learning model for explaining
                params (list): a list of parameter names
                X (numpy array): data to be explainer
                y (numpy array): labels of X
                base_x (numpy array): baseline x used for the SHAP methods family
            Return:
                sensi values (numpy array)
        """
        sensi = learner.model.get_booster().get_score(importance_type="total_gain")
 
        # when use xgboost method family, the output could be dict rather than a numpy array
        sensi_dict = {}
        for k in range(len(params)):
            if 'f' + str(k) in list(sensi.keys()):
                sensi_dict[k] = sensi['f' + str(k)]
            else:
                sensi_dict[k] = 0.0
        sensi = np.nan_to_num(np.array(list(sensi_dict.values())))
        sensi = sensi.squeeze()
        sensi = np.nan_to_num(sensi / np.sum(sensi))
        return sensi
 
 
class XGBTotalCoverExplainer(Explainer):
    @functionLog
    def __init__(self, name:str, seed:int):
        """Initializer, only used for explaining nonlinear learning models
            Args: name (string): name of explaining model.
            use xgboost's self-explaining method, which support :
            - 'weight': the number of times a feature is used to split the data across all trees.
            - 'gain': the average gain across all splits the feature is used in.
            - 'cover': the average coverage across all splits the feature is used in.
            - 'total_gain': the total gain across all splits the feature is used in.
            - 'total_cover': the total coverage across all splits the feature is used in.
            use total_gain as default
        """
        super().__init__(name, seed)
        self.name = name
        if self.name != "XGBTotalCover":
            logger.info("Selected XGBTotalCover but the current explainer {} is specified".format(name))
            self.name = "XGBTotalCover"
        self.seed = seed
 
    @functionLog
    def run(self,
            learner: Any,
            params: List[str],
            X: np.ndarray,
            y: Optional[np.ndarray] = None,
            base_x: Optional[np.ndarray] = None) -> np.ndarray:
        """Implementation explaining nonlinear learning models
            Args:
                learner (model): a learning model for explaining
                params (list): a list of parameter names
                X (numpy array): data to be explainer
                y (numpy array): labels of X
                base_x (numpy array): baseline x used for the SHAP methods family
            Return:
                sensi values (numpy array)
        """
        sensi = learner.model.get_booster().get_score(importance_type="total_cover")
 
        # when use xgboost method family, the output could be dict rather than a numpy array
        sensi_dict = {}
        for k in range(len(params)):
            if 'f' + str(k) in list(sensi.keys()):
                sensi_dict[k] = sensi['f' + str(k)]
            else:
                sensi_dict[k] = 0.0
        sensi = np.nan_to_num(np.array(list(sensi_dict.values())))
        sensi = sensi.squeeze()
        sensi = np.nan_to_num(sensi / np.sum(sensi))
        return sensi

class GPExplainer(Explainer):
    @functionLog
    def __init__(self, name: str, seed:int):
        """Gaussian process based explainer initializer,
        Args: name (string): name of explaining model.
         Use gaussian process regressor to model data, then use marginal variance as sensitivity scores
        """
        super().__init__(name, seed)
        self.name = name
        if name != "GP":
            logger.info("Selected GP but the current explainer {} is specified".format(name))
            self.name = "GP"
        self.seed = seed
 
    @functionLog
    def run(self,
            learner: Any,
            params: List[str],
            X: np.ndarray,
            y: Optional[np.ndarray] = None,
            base_x: Optional[np.ndarray] = None) -> np.ndarray:
        """use iTuned methods for sensitization
        Args:
            learner (model): a learning model for explaining
            params (list): a list of parameter names
            X (numpy array): data to be explainer
            y (numpy array): labels of X
            base_x (numpy array): baseline x used for the SHAP methods family
        Return:
            normalized sensitivity scores
        """
 
        from sklearn.gaussian_process import GaussianProcessRegressor
        from sklearn.metrics import mean_absolute_percentage_error
        from sklearn.model_selection import train_test_split
 
        # split training and testing data for building learning and explaining models
        X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                            train_size=0.75,
                                                            random_state=self.seed)
 
        model = GaussianProcessRegressor(random_state=self.seed, normalize_y=True)
        model.fit(X_train, y_train)
        y_true = y_test.ravel()
        y_pred = model.predict(X_test)
 
        self.performance = mean_absolute_percentage_error(y_true, y_pred)
 
        m = X_train.shape[1]
        var_y = np.std(y_train)
        var_x = np.ones(m)
        for i in range(m):
            vs = np.unique(X_train[:, i])
            y_m = []
            for j in vs:
                X_copy = deepcopy(X_train)
                X_copy[:, i] = j
                y_m.append(
                    np.mean(
                        model.predict(X_copy)
                    )
                )
            var_x[i] = np.std(y_m)
 
        sensi = np.array(var_x) / var_y
        sensi = np.nan_to_num(sensi)
        sensi = sensi / np.sum(np.abs(sensi))
        return sensi
 
 
class LassoExplainer(Explainer):
    @functionLog
    def __init__(self, name: str, seed: int):
        """Initializer for Lasso linear regressor as the explainer.
        Args: name (string): name of explaining model.
        Use lasso regressor to model data, then use the weight values as sensitivity scores
        """
        super().__init__(name, seed)
        self.name = name
        if name != "LASSO":
            logger.info("Selected LASSO but the current explainer {} is specified".format(name))
            self.name = "LASSO"
        self.seed = seed
 
    @functionLog
    def run(self,
            learner: Any,
            params: List[str],
            X: np.ndarray,
            y: Optional[np.ndarray] = None,
            base_x: Optional[np.ndarray] = None) -> np.ndarray:
        """Implement lasso linear regression method for sensitization
        Args:
            learner (model): a learning model for explaining
            params (list): a list of parameter names
            X (numpy array): data to be explainer
            y (numpy array): labels of X
            base_x (numpy array): baseline x used for the SHAP methods family
        Return:
            normalized sensitivity scores
            """
        from sklearn.linear_model import Lasso
        from sklearn.metrics import mean_absolute_percentage_error
        from sklearn.model_selection import train_test_split
 
        # split training and testing data for building learning and explaining models
        X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                            train_size=0.75,
                                                            random_state=self.seed)
 
        model = Lasso(max_iter=10000)
        model.fit(X_train, y_train)
        y_true, y_pred = y_test.ravel(), model.predict(X_test)
 
        self.performance = mean_absolute_percentage_error(y_true, y_pred)
 
        sensi = model.coef_/np.sum(np.abs(model.coef_))
        sensi = np.nan_to_num(sensi)
        return sensi

class MIExplainer(Explainer):
    @functionLog
    def __init__(self, name: str, seed: int):
        """Initializer for mutual information as the explainer.
        Args: name (string): name of explaining model.
        use mutual info regression since it can capture pair-wise linear and nonlinear relation,
        then use the mutual information values as sensitivity scores
        """
        super().__init__(name, seed)
        self.name = name
        if name != "MI":
            logger.info("Selected MI but the current explainer {} is specified".format(name))
            self.name = "MI"
        self.seed = seed
 
    @functionLog
    def run(self,
            learner: Any,
            params: List[str],
            X: np.ndarray,
            y: Optional[np.ndarray] = None,
            base_x: Optional[np.ndarray] = None) -> np.ndarray:
        """Implement mutual info regression based univariate explainer
        Args:
            learner (model): a learning model for explaining
            params (list): a list of parameter names
            X (numpy array): data to be explainer
            y (numpy array): labels of X
            base_x (numpy array): baseline x used for the SHAP methods family
        Return:
            normalized sensitivity scores
        """
        from sklearn.feature_selection import SelectKBest, mutual_info_regression
        score_func = mutual_info_regression
        model = SelectKBest(score_func, k="all")
        model.fit(X, y)
        sensi = np.nan_to_num(model.scores_)
        sensi = sensi / np.sum(np.abs(sensi))
        return sensi