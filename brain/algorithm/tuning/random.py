# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
import numpy

from brain.algorithm.tuning.base import OptimizerUnit


class Random(OptimizerUnit):
    def __init__(self,
                 opt_name: str,
                 max_iteration: int,
                 knobs: list,
                 baseline: dict,
                 rule_list=None):
        super(Random, self).__init__(opt_name, max_iteration, knobs, baseline, rule_list)

    def acquireImpl(self):
        config = {}
        for param in self.knobs:
            if param.__contains__('range'):
                config[param['name']] = numpy.random.randint(
                    param['range'][0], param['range'][1])

            elif param.__contains__('options'):
                config[param['name']] = param['options'][numpy.random.randint(
                    0, param['options'].__len__())]

            elif param.__contains__('sequence'):
                config[param['name']] = param['sequence'][numpy.random.randint(
                    0, param['sequence'].__len__())]

            else:
                raise Exception("unsupported parameter type!")
                
        return config, 1.0

    def feedbackImpl(self, iteration: int, loss: float):
        pass

    def msg(self):
        return "Random"
