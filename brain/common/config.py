# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
import os
import sys
import logging

from logging import config as logconf
from configparser import ConfigParser

LOG_CONF = {
    "version": 1,
    "formatters": {
        "std": {
            "format": "PID=%(process)d %(asctime)s %(levelname)s : %(message)s - %(filename)s:%(lineno)d"
        },
        "simple": {
            "format": "%(asctime)s %(levelname)s : %(message)s"
        }
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "formatter": "simple",
            "stream": sys.stdout
        },
        "logfile": {
            "class": "logging.handlers.TimedRotatingFileHandler",
            "filename": "/var/log/keentune/brain.log",
            "interval": 1,
            "backupCount": 14,
            "level": "DEBUG",
            "formatter": "std",
        }
    },
    "loggers": {
        "common": {
            "level": "DEBUG",
            "handlers": ["console","logfile"],
            "propagate": False
        },
        "auto": {
            "level": "DEBUG",
            "handlers": ["logfile"],
            "propagate": False
        }
    },
    "root": {
        "level": "DEBUG",
        "handlers": ["console"]
    }
}

class Config:
    conf_file_path = "/etc/keentune/brain/brain.conf"
    conf = ConfigParser()
    conf.read(conf_file_path)
    print("Loading config in {}".format(conf_file_path))

    LOG_CONF['handlers']['console']['level']       = conf['log']['CONSOLE_LEVEL']
    LOG_CONF['handlers']['logfile']['level']       = conf['log']['LOGFILE_LEVEL']
    LOG_CONF['handlers']['logfile']['filename']    = conf['log']['LOGFILE_PATH']
    LOG_CONF['handlers']['logfile']['interval']    = int(conf['log']['LOGFILE_INTERVAL'])
    LOG_CONF['handlers']['logfile']['backupCount'] = int(conf['log']['LOGFILE_BACKUP_COUNT'])

    logconf.dictConfig(LOG_CONF)
    logger = logging.getLogger('common')

    WORKSPACE  = conf['brain']['WORKSPACE']
    BRAIN_PORT = conf['brain']['BRAIN_PORT']
    
    # workdir
    SENSI_DATA_PATH = os.path.join(WORKSPACE, 'sensi_data')
    TUNE_DATA_PATH  = os.path.join(WORKSPACE, 'tuning_data')

    # Auto-Tuning
    MAX_SEARCH_SPACE = int(conf['tuning']['MAX_SEARCH_SPACE'])
    SURROGATE = conf['tuning']['SURROGATE']
    STRATEGY  = conf['tuning']['STRATEGY']
    ACQUIRE_TIMEOUT = int(conf['tuning']['ACQUIRE_TIMEOUT'])
    FEEDBACK_TIMEOUT = int(conf['tuning']['FEEDBACK_TIMEOUT'])

    # Sensitize
    EPOCH     = int(conf['sensitize']['EPOCH'])
    TOPN      = int(conf['sensitize']['TOPN'])
    THRESHOLD = float(conf['sensitize']['THRESHOLD'])

    logger.info("keentune-brain workspace: {}".format(WORKSPACE))
    logger.info("keentune-brain listenting port: {}".format(BRAIN_PORT))
    