# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
import json
import logging
import requests
import subprocess


logger = logging.getLogger('common')


def sysCommand(command: str, cwd: str = "./"):
    '''Run system command with subprocess.run and return result
    '''
    result = subprocess.run(
        command,
        shell=True,
        close_fds=True,
        cwd=cwd,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE
    )

    suc = (result.returncode == 0)
    out = result.stdout.decode('UTF-8', 'strict').strip()
    error = result.stderr.decode('UTF-8', 'strict').strip()

    if not suc:
        logger.error("Exec command '{}': {}".format(command, error))
        return suc, error

    else:
        logger.debug("Exec command '{}': {}".format(command, out))
        return suc, out


def httpResponse(response_data, response_ip, response_port, response_api):
    logger.debug("send response to {ip}:{port}:{data}".format(
        ip = response_ip,
        port = response_port,
        data = response_data
    ))

    try:
        requests.post(
            url = "http://{ip}:{port}/{api}".format(ip = response_ip, port = response_port, api = response_api),
            data = json.dumps(response_data),
            timeout = 3)
        
    except requests.exceptions.ConnectTimeout:
        logger.warning("[HTTP] send response to {ip}:{port}/{api} timeout!".format(
            ip = response_ip, port = response_port, api = response_api))