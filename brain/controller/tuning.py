# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
import os
import json
import signal
import logging

from tornado.web import RequestHandler
from brain.controller.process import TuningProcess


logger = logging.getLogger('common')


TUNING_PROC = None


def shutdown():
    global TUNING_PROC
    if TUNING_PROC is None:
        return
    
    TUNING_PROC.teardown()

    try:
        logger.info("kill tuning process: pid = {}".format(TUNING_PROC.pid))
        os.kill(TUNING_PROC.pid, signal.SIGKILL) 
    
    except ProcessLookupError as e:
        logger.warning("can not find tuning process: {}".format(e))
        del TUNING_PROC
        TUNING_PROC = None
    
    else:
        del TUNING_PROC
        TUNING_PROC = None


class InitHandler(RequestHandler):
    def post(self):
        global TUNING_PROC
        request_data = json.loads(self.request.body)
        logger.debug("initailize request: post: {}".format(request_data))

        try:
            shutdown()
        except Exception as e:
            logger.error("fail to kill tuning process: {}".format(e))
            self.write(json.dumps({
                "suc": False,
                "msg": "fail to kill tuning process: {}".format(e)}))
            self.finish()
        
        try:
            TUNING_PROC = TuningProcess(
                name = request_data["name"],
                algorithm = request_data["algorithm"],
                iteration = request_data["iteration"],
                parameters = request_data["parameters"],
                baseline = request_data["baseline_score"],
                rule_list = request_data["rule_list"]
            )
            TUNING_PROC.start()

            # Get csv file head after initialzation.
            head_param, head_bench, head_time = TUNING_PROC.out_q[1].recv()

        except Exception as e:
            logger.error("initailize optimizer process failed:{}".format(e))
            self.write(json.dumps({
                "suc": False,
                "msg": "Initailize optimizer process failed:{}".format(e)}))
            self.finish()

        else:
            logger.debug("success to initailize tuning process: {} {} {}".format(
                head_param, head_bench, head_time
            ))
            self.write(json.dumps({
                "suc": True,
                "msg": "",
                "parameters_head" : head_param,
                "score_head"      : head_bench,
                "time_head"       : head_time
            }))
            self.finish()


class AcquireHandler(RequestHandler):
    def get(self):
        global TUNING_PROC
        logger.debug("acquire request: get")

        if TUNING_PROC is None or not TUNING_PROC.is_alive():
            logger.warning("no tuning process running")
            self.write("no tuning process running")
            self.finish()
            return

        try:
            TUNING_PROC.cmd_q[0].send("acquire")
            iteration, candidate, budget = TUNING_PROC.out_q[1].recv()

        except Exception as e:
            logger.error("acquire failed: {}".format(e))
            self.write("acquire failed:{}".format(e))
            self.finish()
        
        else:
            logger.info("acquire response: {}".format(candidate))
            response_data = {
                "iteration" : iteration,
                "candidate" : candidate,
                "budget"    : budget,
                "parameter_value" : ",".join([str(param['value']) for param in candidate])
            }
            self.write(json.dumps(response_data))
            self.finish()


class FeedbackHandler(RequestHandler):
    def post(self):
        global TUNING_PROC

        request_data = json.loads(self.request.body)
        logger.debug("feedback request: post: {}".format(request_data))

        if TUNING_PROC is None or not TUNING_PROC.is_alive():
            logger.warning("no tuning process running")
            self.write("no tuning process running")
            self.finish()
            return

        try:
            TUNING_PROC.input_q[0].send((request_data['iteration'], request_data['bench_score']))
            TUNING_PROC.cmd_q[0].send("feedback")
            time_data_line, benchmark_value_line = TUNING_PROC.out_q[1].recv()

        except Exception as e:
            logger.error("feedback failed: {}".format(e))
            self.write(json.dumps({
                "suc": False,"msg": "{}".format(e),"time_data"  : "","score_data" : ""}))
            self.finish()

        else:
            logger.info("feedback accept: {}".format(request_data['bench_score']))
            self.write(json.dumps({
                "suc" : True,"msg" : "","time_data":time_data_line, "score_data":benchmark_value_line}))
            self.finish()


class EndHandler(RequestHandler):
    def get(self):
        try:
            shutdown()
        except Exception as e:
            logger.error("fail to kill tuning process: {}".format(e))


class BestHandler(RequestHandler):
    def get(self):
        global TUNING_PROC
        logger.debug("best request: get")

        if TUNING_PROC is None or not TUNING_PROC.is_alive():
            logger.warning("no tuning process running")
            self.write("no tuning process running")
            self.finish()
            return

        try:
            TUNING_PROC.cmd_q[0].send("best")
            best_iteration, best_candidate, best_bench = TUNING_PROC.out_q[1].recv()

        except Exception as e:
            logger.error("get best config failed:{}".format(e))
            self.write("get best config failed:{}".format(e))
            self.finish()
            
        else:
            logger.info("response best config: {} {} {}".format(best_iteration, best_candidate, best_bench))

            response_data = {
                "iteration"  : best_iteration,
                "candidate"  : best_candidate,
                "bench_score": best_bench
            }
            self.write(json.dumps(response_data))
            self.finish()
