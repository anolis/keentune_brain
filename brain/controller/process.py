# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
import os
import pickle 
import logging

from multiprocessing import Process, Pipe

from brain.common.system import httpResponse
from brain.common.config import Config
from brain.algorithm.sensitize.sensitize import sensitize

logger = logging.getLogger('common')


class TuningProcess(Process):
    def __init__(self, 
                name, 
                algorithm, 
                iteration, 
                parameters, 
                baseline, 
                rule_list):
        
        super(TuningProcess, self).__init__()
        self.cmd_q, self.out_q, self.input_q = Pipe(), Pipe(), Pipe()

        if algorithm.lower() == "hord":
            from brain.algorithm.tuning.hord import HORD
            self.optimizer = HORD(name, iteration, parameters, baseline,rule_list)

        elif algorithm.lower() == "random":
            from brain.algorithm.tuning.random import Random
            self.optimizer = Random(name, iteration, parameters, baseline,rule_list)
        
        elif algorithm.lower() == "lamcts":
            from brain.algorithm.tuning.lamcts import LamctsOptim
            self.optimizer = LamctsOptim(name, iteration, parameters, baseline,rule_list)

        elif algorithm.lower() == "bgcs":
            from brain.algorithm.tuning.bgcs import BgcsOptim
            self.optimizer = BgcsOptim(name, iteration, parameters, baseline,rule_list)

        elif algorithm.lower() == "tpe":
            from brain.algorithm.tuning.tpe import TPE
            self.optimizer = TPE(name, iteration, parameters, baseline,rule_list)

        else:
            raise Exception("invalid algorithom: {}".format(algorithm))

        head_param, head_bench, head_time = self.optimizer.getDataHead()
        self.out_q[0].send((head_param, head_bench, head_time))

    def run(self):
        ''' process.start() '''
        logger.info("Create tuning process, pid = {}".format(self.pid))

        while True:
            cmd = self.cmd_q[1].recv()
            if cmd == "acquire":
                try:
                    iteration, candidate, budget = self.optimizer.acquire()
                    logger.info("acquire candidate: {}".format(candidate))
                    self.out_q[0].send((iteration, candidate, budget))

                except Exception as e:
                    logger.critical("acquire error: {}".format(e))
                    os._exit(1)

            elif cmd == "feedback":
                try:
                    iteration, bench_score = self.input_q[1].recv()
                    time_data_line, benchmark_value_line = self.optimizer.feedback(
                        iteration = iteration, bench_score = bench_score)
                    logger.info("feedback benchmark: {}".format(bench_score))
                    self.out_q[0].send((time_data_line, benchmark_value_line))

                except Exception as e:
                    logger.critical("feedback error: {}".format(e))
                    os._exit(1)

            elif cmd == "best":
                try:
                    best_iteration, best_candidate, best_bench = self.optimizer.best()
                    logger.info("get best candidate: {}".format(best_candidate))
                    self.out_q[0].send((best_iteration, best_candidate, best_bench))

                except Exception as e:
                    logger.critical("get best error: {}".format(e))
                    os._exit(1)
        os._exit(0)

    def teardown(self):
        del self.optimizer
        self.cmd_q[0].close()
        self.cmd_q[1].close()
        self.out_q[0].close()
        self.out_q[1].close()
        self.input_q[0].close()
        self.input_q[1].close()


class SensitizeProcess(Process):
    def __init__(self,
                data_name,
                trials,
                explainer,
                response_ip,
                response_port):

        super(SensitizeProcess, self).__init__()

        self.data_name = data_name
        self.trials = trials
        self.explainer = explainer
        self.response_ip = response_ip
        self.response_port = response_port

    def run(self):
        ''' process.start() '''
        logger.info("sensitize process running, pid = {}".format(self.pid))

        try:
            sensitize_result, sensi_file = sensitize(
                data_name = self.data_name, 
                trials    = self.trials, 
                explainer = self.explainer, 
                epoch     = Config.EPOCH, 
                topN      = Config.TOPN,
                threshold = Config.THRESHOLD
            )
            logger.info("sensitize result: {}".format(sensitize_result))

            head = ",".join([i['name'] for i in sensitize_result])
            data = pickle.load(open(sensi_file,'rb')).tolist()
            response_data = {"suc": True, "head": head, "result": data, "msg": ""}

        except Exception as e:
            logger.error("sensitize error: {}".format(e))
            response_data = {"suc": False, "head": "", "result": [], "msg": "{}".format(e)}
            os._exit(1)
        
        httpResponse(response_data, self.response_ip, self.response_port, "sensitize_result")
        os._exit(0)