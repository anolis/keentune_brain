# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
import os
import json
import signal
import logging

from tornado.web import RequestHandler
from brain.common.dataset import listData, deleteFile
from brain.controller.process import SensitizeProcess

logger = logging.getLogger('common')

SENSI_PROC = None


def shutdown():
    global SENSI_PROC
    if SENSI_PROC is None:
        return
    
    try:
        logger.info("kill sensitize process: pid = {}".format(SENSI_PROC.pid))
        os.kill(SENSI_PROC.pid, signal.SIGKILL) 
    
    except ProcessLookupError as e:
        logger.warning("can not find sensitize process: {}".format(e))
        del SENSI_PROC
        SENSI_PROC = None
        
    else:
        del SENSI_PROC
        SENSI_PROC = None


class SensitizeHandler(RequestHandler):
    def post(self):
        global SENSI_PROC

        request_data = json.loads(self.request.body)
        logger.debug("get sensitize request: {}".format(request_data))

        try:
            shutdown()
        except Exception as e:
            logger.error("fail to kill sensitize process: {}".format(e))
            self.write(json.dumps({
                "suc": False,
                "msg": "fail to kill sensitize process: {}".format(e)}))
            self.finish()

        try:
            SENSI_PROC = SensitizeProcess(
                trials      = int(request_data['trials']),
                data_name   = request_data['data'],
                explainer   = request_data['explainer'],
                response_ip = request_data['resp_ip'],
                response_port = request_data['resp_port']
            )
            SENSI_PROC.start()
        
        except Exception as e:
            logger.error("Failed to start sensitize process: {}".format(e))
            self.write(json.dumps({
                "suc" : False, 
                "msg": "Failed to start sensitize process: {}".format(e)}))
            self.finish()

        else:
            logger.info("sensitize response, process running")
            self.write(json.dumps({"suc" : True, "msg": ""}))
            self.finish()


class TerminateHandler(RequestHandler):
    def get(self):
        try:
            shutdown()
        except Exception as e:
            logger.error("fail to kill sensitize process: {}".format(e))


class DataDeleteHandler(RequestHandler):
    def post(self):
        try:
            data = json.loads(self.request.body)
            logger.info("delete data requests: {}".format(data))
            deleteFile(data['data'])

        except Exception as e:
            logger.error("delete data error: {}".format(e))
            self.write(json.dumps({"suc": False, "msg": "{}".format(e)}))
            self.finish()

        else:
            self.write(json.dumps({"suc": True, "msg": ""}))
            self.finish()


class AvaliableHandler(RequestHandler):
    def get(self):
        try:
            data_list = listData()
            logger.info("get avaliable data: {}".format(data_list))

        except Exception as e:
            logger.error("get avaliable error: {}".format(e))
            self.write(json.dumps({"suc": False, "msg": "{}".format(e)}))
            self.finish()
        
        else:
            self.write(json.dumps({
                "suc"   : True,
                "data"  : data_list,
                "tune"  : ["random", "hord", 'TPE', "lamcts", "bgcs"],
                "explainer" :['Xsen', 'SHAPKernel', 'XGBTotalGain', 'LASSO', 'MI', 'GP']
            }))
            self.finish()
