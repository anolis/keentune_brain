# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
import requests
import unittest


class TestBrainTerminate(unittest.TestCase):
    def setUp(self) -> None:
        self.proxies={"http": None, "https": None}
        url = "http://{}:{}/avaliable".format("localhost", "9872")
        re = requests.get(url, proxies=self.proxies)
        if re.status_code != 200:
            print("ERROR: Can't reach KeenTune-Brain.")
            exit()

    def tearDown(self) -> None:
        pass

    def test_brain_server_FUN_terminate(self):
        url = "http://{}:{}/{}".format("localhost", "9872", "terminate")
        result = requests.get(url, proxies=self.proxies)
        self.assertEqual(result.status_code, 200)
