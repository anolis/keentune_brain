# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
import json
import requests
import unittest


class TestBrainAvailable(unittest.TestCase):
    def test_brain_server_FUN_available(self):
        self.proxies={"http": None, "https": None}
        url = "http://{}:{}/{}".format("localhost", "9872", "avaliable")
        result = requests.get(url, proxies=self.proxies)
        self.assertEqual(result.status_code, 200)
        self.assertIn('{"suc": true', result.text)
