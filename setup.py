# Copyright (c) 2021-2023 Alibaba Cloud Computing Ltd.
# SPDX-License-Identifier: MulanPSL-2.0
from setuptools import setup, find_packages

setup(
    name             = "keentune-brain",
    version          = "2.4.1",
    description      = "KeenTune brain unit",
    url              = "https://gitee.com/anolis/keentune_brain",
    license          = "MulanPSLv2",
    packages         = find_packages(exclude=["test"]),
    package_data     = {'brain': ['brain.conf']},
    python_requires  = '>=3.6',
    long_description = "",

    classifiers = [
        "Environment:: KeenTune",
        "IntendedAudience :: Information Technology",
        "IntendedAudience :: System Administrators",
        "License :: OSI Approved :: MulanPSLv2",
        "Operating System :: POSIX :: Linux",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3.6",
        "ProgrammingLanguage :: Python"
    ],

    data_files  = [
        ("/etc/keentune/brain", ["brain/brain.conf"]),
        ("/var/log/keentune",[]),
        ("/var/keentune/brain/sensi_data",[]),
        ("/var/keentune/brain/tuning_data",[]),
    ],
    
    entry_points = {
        'console_scripts': ['keentune-brain=brain.brain:main']
    }
)
